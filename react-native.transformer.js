'use strict';

const stylus = require('stylus');
const { default: css2rn } = require('css-to-react-native-transform');
const upstreamTransformer = require('metro-react-native-babel-transformer');

/**
 *
 * @param source
 * @param filename
 * @returns {Promise<any>|Promise<T>}
 */
function stylusCompiler(source, filename) {
  return new Promise((resolve, reject) => {
    return stylus(source)
      .set('filename', filename)
      .set('paths', [__dirname, __dirname + '/src'])
      .render(function(err, css) {
        // logic
        if (err) {
          return reject(err);
        }
        resolve(css);
      });
  }).then(css => css2rn(css, { parseMediaQueries: false }));
}

module.exports.transform = async function transform({
  src,
  filename,
  options,
}) {
  if (filename.endsWith('.styl')) {
    const stylesheet = await stylusCompiler(src, filename);
    return upstreamTransformer.transform({
      src: 'module.exports = ' + JSON.stringify(stylesheet),
      filename,
      options,
    });
  }

  return upstreamTransformer.transform({ src, filename, options });
};

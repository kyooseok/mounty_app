/**
 * @flow
 */
import { autobind } from 'core-decorators';
import { observable, action, computed, reaction } from 'mobx';
import { asyncAction } from 'mobx-utils';

import AppCache, { ASK_ACCESS_TOKEN } from 'app/cache';
import RestClientService from 'services/RestClientService';
import { SessionService } from 'services/apis/endpoints/accounts';

@autobind
export class SessionModel {
  /**
   * 서버로 부터 발급 된 토큰
   *
   * @type {?string}
   */
  @observable accessToken: string = null;

  /**
   *
   * @returns {boolean}
   */
  @computed get isAuthenticated() {
    return !!this.accessToken;
  }

  constructor() {
    reaction(
      () => this.accessToken,
      () => {
        RestClientService.setJwtToken(this.accessToken);
      },
    );
  }

  /**
   * 서버로 부터 발급 된 토큰을 저장
   *
   * @param {string} accessToken
   */
  @action setAccessToken(accessToken: string): SessionModel {
    this.accessToken = accessToken;
    return this;
  }

  /**
   * @params {string} username
   * @params {string} password
   *
   * @returns {AsyncGenerator<*, void, ?>}
   */
  @asyncAction
  async *createAsync(username: string, password: string) {
    const response = yield SessionService.createAsync({
      body: {
        userid: username,
        passwd: password,
      },
    });

    const accessToken = String(response?.accessToken);

    AppCache.set(ASK_ACCESS_TOKEN, accessToken);
    this.setAccessToken(accessToken);

    return accessToken;
  }

  @asyncAction
  async *destroyAsync() {
    this.setAccessToken(null);
    AppCache.remove(ASK_ACCESS_TOKEN);

    yield new Promise(resolve => setTimeout(resolve, 300));

    return true;
  }

  @asyncAction
  async *hydrateAsync() {
    const accessToken = AppCache.get(ASK_ACCESS_TOKEN);

    try {
      if (accessToken) {
        // 캐시에서 토큰을 가져와서 자동로그인 시킴
        const response = yield SessionService.pingAsync({
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });

        if (response?.error) {
          return null;
        }

        this.setAccessToken(accessToken);
        return accessToken;
      }
    }
    catch(e) {
      console.warn(e);
    }

    return null;
  }
}

export default new SessionModel();

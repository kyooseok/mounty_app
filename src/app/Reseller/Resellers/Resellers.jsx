// @flow
import React from 'react';
import { FlatList } from 'react-native';
import { useQuery } from 'react-query';

import { isEmpty, orderBy } from 'lodash';
import urlParse from 'url-parse';
import stubArray from 'utils/stubArray';
import RouterService from 'services/RouterService';
import RestClientService from 'services/RestClientService';

import {
  ItemRow,
  ItemRowSeparator,
  ListHeaderComponent,
  PartnerItemPropTypes,
} from './ResellersPresenter';
import './styles.styl';

const RESOURCE_CONFIG = {
  ITEM_HEIGHT: 74,
  IN_APP_WEB_VIEW_HOST: ['CS.WLFA.US'],
};

function queryFn(): Promise<[PartnerItemPropTypes]> {
  return RestClientService.get('/api/thehanapp/reseller')
    .then(response => response.body)
    .then(({ data }) => {
      return stubArray(data.items).reduce((acc, item) => {
        if (item.hideYN === 'N') {
          item.order = parseInt(item.order, 10) || 999999;
          acc.push(item);
        }
        return acc;
      }, []);
    })
    .then(data => orderBy(data, ['order', 'asc']));
}

export default function Resellers(props = {}) {
  const { status, data } = useQuery('Partners', queryFn);

  /**
   *
   * @param item
   */
  const onPressItem = (item: PartnerItemPropTypes) => {
    if (isEmpty(item.detailURL)) {
      return null;
    }

    const hostname = urlParse(item.detailURL).hostname;

    if (RESOURCE_CONFIG.IN_APP_WEB_VIEW_HOST.includes(hostname.toUpperCase())) {
      return RouterService.showWebViewModal({
        uri: item.detailURL,
        title: item.name,
      });
    }

    return RouterService.openInAppBrowser(item.detailURL);
  };

  const itemRenderFn = ({ item }) => (
    <ItemRow
      item={item}
      height={RESOURCE_CONFIG.ITEM_HEIGHT}
      onPress={() => onPressItem(item)}
    />
  );

  const getItemLayoutFn = (_, index) => ({
    index,
    length: RESOURCE_CONFIG.ITEM_HEIGHT,
    offset: RESOURCE_CONFIG.ITEM_HEIGHT * index,
  });

  return (
    status === 'success' && (
      <FlatList
        data={data}
        keyExtractor={item => item.name}
        renderItem={itemRenderFn}
        initialNumToRender={3}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => <ItemRowSeparator />}
        getItemLayout={getItemLayoutFn}
        ListHeaderComponent={<ListHeaderComponent />}
      />
    )
  );
}

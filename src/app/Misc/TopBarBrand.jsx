import React from 'react';
import { Image } from 'react-native';

export default function TopBarBrand() {
  return (
    <Image
      source={require('assets/images/mounty_ci.png')}
      resizeMode="contain"
      style={{ width: 80, height: 19 }}
    />
  );
}

/**
 *
 * @flow
 */
import React from 'react';
import { InteractionManager, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Navigation } from 'react-native-navigation';

import Transactions from './Transactions';

import 'styles/implement.styl';

export default class TransactionsScreen extends React.Component {
  static options() {
    return {
      topBar: {
        title: {
          text: '마감거래내역',
        },
      },
    };
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  componentDidMount(): void {
    const { componentId, commandType } = this.props;

    if (commandType === 'showModal') {
      InteractionManager.runAfterInteractions(() => {
        MaterialCommunityIcons.getImageSource('close', 26, '#FFFFFF').then(
          icon => {
            Navigation.mergeOptions(componentId, {
              topBar: {
                rightButtons: [
                  {
                    id: 'DISMISS_MODAL',
                    icon: icon,
                  },
                ],
              },
            });
          },
        );
      });
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  render() {
    return (
      <View styleName="compacted bg-background">
        {React.createElement(Transactions, this.props)}
      </View>
    );
  }
}

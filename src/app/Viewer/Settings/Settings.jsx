// @flow
import React from 'react';
import { View, Text } from 'react-native';

import { useObserver } from 'mobx-react';
import { Touchable } from 'components/Button';
import RouterService from 'services/RouterService';
import useObservable from 'utils/hooks/useObservable';
import useQueryViewerProfile from 'utils/hooks/useQueryViewerProfile';
import InterlockEnableSwitch from './InterlockEnableSwitch';
import LogoutAction from './LogoutAction';

import './styles.styl';

function useSettings() {
  const { session } = useObservable();
  const query = useQueryViewerProfile();

  return useObserver(() => {
    return {
      isAuthenticated: session.isAuthenticated,
      ...query,
    };
  });
}

export default function Settings(props = {}) {
  const { isAuthenticated, data, status, refetch } = useSettings();

  React.useEffect(() => {
    if (isAuthenticated === false) {
      refetch();
    }
  }, [refetch, isAuthenticated]);

  return (
    status === 'success' && (
      <React.Fragment>
        <View styleName="Section">
          <View styleName="SectionTitle">
            <Text styleName="SectionTitleText">연동상태</Text>
          </View>

          <View styleName="ItemRow">
            <View styleName="ItemRowTitle">
              <Text styleName="ItemRowTitleText">로직</Text>
            </View>
            <Text styleName="text13 regular caption">
              {data.trading_manager}
            </Text>
          </View>
          <View styleName="ItemRow">
            <View styleName="ItemRowTitle">
              <Text styleName="ItemRowTitleText">연동비</Text>
            </View>
            <Text styleName="text13 regular caption numeric">
              {data.trading_ratio}%
            </Text>
          </View>
          <View styleName="ItemRow">
            <View styleName="ItemRowTitle">
              <Text styleName="ItemRowTitleText">연동 설정</Text>
            </View>
            <InterlockEnableSwitch
              userdata={data}
              disabled={isAuthenticated === false}
            />
          </View>
        </View>

        {/* {isAuthenticated && (
          <View styleName="Section">
            <View styleName="SectionTitle">
              <Text styleName="SectionTitleText">계약사항</Text>
            </View>

            <View styleName="ItemRow">
              <View styleName="ItemRowTitle">
                <Text styleName="ItemRowTitleText">판매사</Text>
              </View>
              <Text styleName="text13 regular caption">
                {data.sales_company}
              </Text>
            </View>
          </View>
        )} */}

        <View styleName="Section">
          <View styleName="SectionTitle">
            <Text styleName="SectionTitleText">약관</Text>
          </View>

          <Touchable
            activeOpacity={0.9}
            onPress={() =>
              RouterService.showWebViewModal({
                uri: 'https://thehantrader.com/app/terms_of_use',
              })
            }>
            <View styleName="ItemRow">
              <View styleName="ItemRowTitle">
                <Text styleName="ItemRowTitleText">서비스 이용약관</Text>
              </View>
            </View>
          </Touchable>
          <Touchable
            activeOpacity={0.9}
            onPress={() =>
              RouterService.showWebViewModal({
                uri: 'https://thehantrader.com/app/privacy',
              })
            }>
            <View styleName="ItemRow">
              <View styleName="ItemRowTitle">
                <Text styleName="ItemRowTitleText">개인정보 취급방침</Text>
              </View>
            </View>
          </Touchable>
        </View>

        <LogoutAction styleName="ItemRow mt35 mb25">
          <View styleName="ItemRowTitle">
            <Text styleName="ItemRowTitleText">로그아웃</Text>
          </View>
        </LogoutAction>
      </React.Fragment>
    )
  );
}

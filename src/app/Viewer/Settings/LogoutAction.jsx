// @flow
import React from 'react';
import { Alert, TextStyle, StyleProp } from 'react-native';
import { useObserver } from 'mobx-react';

import { Touchable, TouchablePropTypes } from 'components/Button';
import useObservable from 'utils/hooks/useObservable';

type LogoutPropTypes = $Rest<TouchablePropTypes, {| onPress?: any |}> & {
  children: any,
  textStyle?: StyleProp<TextStyle>,
};
export default function LogoutAction(props: LogoutPropTypes = {}) {
  const { session } = useObservable();

  /**
   *
   */
  const onPressed = () => {
    Alert.alert(
      '안내',
      '로그아웃 하시겠습니까?',
      [
        {
          text: '취소',
          style: 'cancel',
        },
        { text: '로그아웃', onPress: () => session.destroyAsync() },
      ],
      { cancelable: false },
    );
  };

  return useObserver(() => {
    return session.isAuthenticated ? (
      <Touchable onPress={onPressed} {...props}>
        {props.children}
      </Touchable>
    ) : null;
  });
}

// @flow
import React from 'react';
import { View, Text } from 'react-native';

import Touchable from 'components/Button/Touchable';
import RouterService from 'services/RouterService';
import useQueryViewer from 'utils/hooks/useQueryViewer';

import {
  Heading,
  LinkingAccount,
  BalanceSummary,
  PortfolioList,
} from './BalancePresenter';

import './styles.styl';

type BalanceFragmentPropTypes = {
  appeared: boolean,
  componentId: string,
};
function Balance(props: BalanceFragmentPropTypes = {}) {
  const { data, status } = useQueryViewer({
    refetchInterval: props.appeared ? 5000 : false,
  });

  return (
    status === 'success' && (
      <View styleName="bg-white">
        <View styleName="Welcome">
          <Text styleName="text19 regular">
            <Text styleName="medium">{data.username}</Text>
            님, 안녕하세요!
          </Text>
        </View>

        {/* 계좌정보 */}
        <LinkingAccount security={data.security} account_no={data.account_no} />

        {/* 잔고 */}
        <BalanceSummary {...data.balance} />
        <View styleName="m15">
          <Text styleName="text13 regular caption">
            추정자산총액과 증권사 HTS의 추정자산총액은 일부 오차가 있을 수
            있습니다.
          </Text>
        </View>

        {/* 보유종목 */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingTop: 35,
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 10,
          }}>
          <Heading text="보유종목" />
          <Touchable
            onPress={() => RouterService.showViewerTransactionsModal()}>
            <Text styleName="text16 regular link">마감거래보기</Text>
          </Touchable>
        </View>
        <PortfolioList
          portfolio={data.portfolio}
          ListFooterComponent={() => (
            <View styleName="m35 mb50">
              <Text styleName="text13 regular caption text-align-center">
                증권사 수수료와 거래세가 공제되지 않았으며{'\n'}
                실제 손익과 일부 차이가 있을 수 있습니다.
              </Text>
            </View>
          )}
        />
      </View>
    )
  );
}

export default React.memo(Balance);

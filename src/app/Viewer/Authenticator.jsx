// @flow
import React from 'react';
import {
  Platform,
  Image,
  ImageBackground,
  Text,
  View,
  Button
} from 'react-native';
import { useObserver } from 'mobx-react';
import { useDimensions } from '@react-native-community/hooks';

import Guide from 'components/Guide/Guide';
import useObservable from 'utils/hooks/useObservable';

import 'styles/implement.styl';

type AuthenticatorPropTypes = {
  children: any,
};
export default function Authenticator(props: AuthenticatorPropTypes = {}) {
  const { session } = useObservable();

  return useObserver(() => {
    return session.isAuthenticated ? props.children : <RequiresLogin />;
  });
}

// function RequiresLoginImageBackground(props = {}) {
//   const { window } = useDimensions();
//   const [imageHeight, setImageHeight] = React.useState(0);
//   const resolvedAssetSource = Image.resolveAssetSource(
//     require('assets/images/balance_placeholder.png'),
//   );

//   const imageRatio = resolvedAssetSource.height / resolvedAssetSource.width;
//   const blurRadius = Platform.select({
//     ios: 12,
//     android: 3,
//   });

//   React.useEffect(() => {
//     setImageHeight(imageRatio * window.width);
//   }, [imageRatio, window.width]);

//   return (
//     <ImageBackground
//       source={require('assets/images/balance_placeholder.png')}
//       blurRadius={blurRadius}
//       resizeMode="stretch"
//       style={{
//         width: window.width,
//         height: imageHeight,
//         opacity: 0.7,
//         backgroundColor: 'white',
//       }}
//     />
//   );
// }

function RequiresLogin() {
  return <Guide />
}

// 로그인이 안된 사용자화면
// function RequiresLogin(props = {}) {
//   return (
//     <View styleName="compacted bg-white">
//       <View styleName="p25">
//         <Text styleName="text19 regular">
//           {'안녕하세요?\n로그인 후 연동 된 계좌를 확인해보세요!'}
//         </Text>
//         <Button
//           title='로그인'
//           mode="contained"
//           activeOpacity={0.9}
//           onPress={() => RouterService.showLoginModal()}
//           styleName="bg-primary mt15">
//           <Text styleName="text16 medium white">로그인</Text>
//         </Button>
//       </View>
//       <View style={{ flex: 1, overflow: 'hidden' }}>
//         <RequiresLoginImageBackground />
//       </View>
//     </View>
//   );
// }

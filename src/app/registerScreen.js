'use strict';

import { Navigation } from 'react-native-navigation';
import {
  TOP_BAR_BRAND,
  WEB_VIEW_SCREEN,
  ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN,
  //
  BROWSE_SCREEN,
  FOREIGN_SCREEN,
  HOME_SCREEN,
  MANAGER_RANKS_SCREEN,
  RESELLERS_SCREEN,
  LOGIN_SCREEN,
  CONTACTS_SCREEN,
  VIEWER_PROFILE_SCREEN,
  VIEWER_SETTINGS_SCREEN,
  VIEWER_TRANSACTIONS_SCREEN,
  //
  NEWBIE_GUIDE_SCREEN,
} from 'app/routeKeys';

import GuideNewbie from './GuideNewbie/GuideNewbie';
import TopBarBrand from './Misc/TopBarBrand';
import WebViewScreen from './WebView/Screen';
import ForeignScreen from './Browse/BrowseForeignScreen';
import ASK_CopyTradingSystemProductScreen from './ASP_CopyTradingSystem/ASK_CopyTradingSystemProductScreen';

//===
import HomeScreen from './Home/HomeScreen';
import BrowseScreen from './Browse/BrowseScreen';
import RanksScreen from './Manager/RanksScreen';
import ResellersScreen from './Reseller/ResellersScreen';

import LoginScreen from './Login/LoginScreen';
import ContactsModal from './Contacts/ContactsModal';

import Viewer__ProfileScreen from './Viewer/ProfileScreen';
import Viewer__SettingsScreen from './Viewer/SettingsScreen';
import Viewer__TransactionsScreen from './Viewer/TransactionsScreen';

const RouteMap = new Map();
RouteMap.set(BROWSE_SCREEN, () => GuideNewbie);
RouteMap.set(NEWBIE_GUIDE_SCREEN, () => GuideNewbie);
RouteMap.set(FOREIGN_SCREEN, () => ForeignScreen);
RouteMap.set(TOP_BAR_BRAND, () => TopBarBrand);
RouteMap.set(WEB_VIEW_SCREEN, () => WebViewScreen);

RouteMap.set(
  ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN,
  () => ASK_CopyTradingSystemProductScreen,
);

////
RouteMap.set(HOME_SCREEN, () => HomeScreen);
RouteMap.set(BROWSE_SCREEN, () => BrowseScreen);
RouteMap.set(RESELLERS_SCREEN, () => ResellersScreen);
RouteMap.set(MANAGER_RANKS_SCREEN, () => RanksScreen);

RouteMap.set(LOGIN_SCREEN, () => LoginScreen);
RouteMap.set(CONTACTS_SCREEN, () => ContactsModal);
RouteMap.set(VIEWER_PROFILE_SCREEN, () => Viewer__ProfileScreen);
RouteMap.set(VIEWER_SETTINGS_SCREEN, () => Viewer__SettingsScreen);
RouteMap.set(VIEWER_TRANSACTIONS_SCREEN, () => Viewer__TransactionsScreen);

export default function registerScreen() {
  RouteMap.forEach(function(generator, componentName) {
    Navigation.registerComponent(componentName, generator);
  });
}

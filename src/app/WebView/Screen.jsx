import React from 'react';
import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';

import WebView from 'components/WebView';
import UIContainer from 'components/UIContainer';

export default class WebViewScreen extends React.Component {
  static options(props = {}) {
    let navigationOptions = Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          height: 44,
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });

    navigationOptions.sideMenu = {
      left: { enabled: false },
    };

    if (props.commandType === 'showModal') {
      navigationOptions.topBar.rightButtons = [
        {
          id: 'DISMISS_MODAL',
          icon: require('assets/images/icons/ic_close.png'),
        },
      ];
    }

    return navigationOptions;
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  render() {
    const { uri, componentId } = this.props;

    return (
      <UIContainer>
        <WebView source={{ uri }} componentId={componentId} />
      </UIContainer>
    );
  }
}

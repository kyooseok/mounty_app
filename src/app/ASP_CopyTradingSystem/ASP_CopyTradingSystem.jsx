import React from 'react';
import { Navigation } from 'react-native-navigation';

import urlParse from 'url-parse';
import WebView from 'components/WebView';
import { WEB_VIEW_SCREEN, ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN } from 'app/routeKeys';

const BASE_WEBVIEW_HOST = 'thehantrader.com';
const BASE_WEBVIEW_URL = 'https://thehantrader.com/cts_tab/cts_list';
// const BASE_WEBVIEW_URL = 'https://mgrg.joins.com/Cont/Article?articleUrl=https://mnews.joins.com/article/23703793';

export default function ASP_CopyTradingSystem(props={}) {
  const { componentId } = props
    , [ isLoaded, setLoaded ] = React.useState(false);

  /**
   *
   * @param e
   */
  const onNavigationStateChange = (e) => {
    setLoaded(e.loading === false);
  };

  const onShouldStartLoadWithRequest = (e) => {
    if( isLoaded ) {
      const { host, pathname } = urlParse(e.url);

      const componentName =
        (host === BASE_WEBVIEW_HOST) && String(pathname).startsWith('/cts_tab/cts_detail')
          ? ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN
          : WEB_VIEW_SCREEN;

      setImmediate(() => {
        return Navigation.push(componentId, {
          component: {
            name: componentName,
            passProps: {
              uri: e.url,
            },
            options: {
              topBar: {
                title: {
                  component: {
                    name: 'TOP_BAR_BRAND',
                  }
                },
              }
            }
          }
        })
      });

      return false;
    }

    // 아무런 액션을 하지 않음
    return true;
  };

  return (
    <WebView
      componentId={componentId}
      source={{uri: BASE_WEBVIEW_URL}}
      onNavigationStateChange={onNavigationStateChange}
      onShouldStartLoadWithRequest={onShouldStartLoadWithRequest}
      cacheMode="LOAD_NO_CACHE"
      androidHardwareAccelerationDisabled={false}
    />
  )
}


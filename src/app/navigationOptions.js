import { Platform } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function navigationOptions() {
  const animationEnabled =
    Platform.OS === 'ios' ||
    (Platform.OS === 'android' && Platform.Version > 28);

  return Promise.all([
    MaterialCommunityIcons.getImageSource('arrow-left', 26),
  ]).then(icons => {
    return {
      statusBar: {
        style: 'light',
        backgroundColor: '#d2242a',
      },
      layout: {
        orientation: ['portrait'], // An array of supported orientations
        componentBackgroundColor: '#FFFFFF', // Set background color only for components, helps reduce overdraw if background color is set in default options.
        backgroundColor: '#FFFFFF', // Or whatever your screen's background color is
      },
      topBar: {
        visible: true,
        animate: true,
        drawBehind: false,
        // Shadow
        noBorder: true,
        elevation: 0,
        height: 50,
        //= Shadow
        // showAsAction: 'always',
        leftButtonColor: '#FFFFFF',
        rightButtonColor: '#FFFFFF',
        title: {
          color: '#FFFFFF',
          ...Platform.select({
            android: {
              fontSize: 17,
              fontFamily: 'NotoSansKR-Medium',
              // alignment: 'center',
            },
            ios: {
              fontSize: 17,
              fontFamily: 'NotoSansKR-Medium',
            },
          }),
        },
        subtitle: {
          color: '#FFFFFF',
        },
        backButton: {
          icon: icons[0],
          color: '#FFFFFF',
          showTitle: false,
        },
        background: {
          color: '#d2242a',
        },
      },
      bottomTabs: {
        animate: true,
        drawBehind: false,
        backgroundColor: '#FFFFFF',
        hideShadow: false,

        // Only android
        elevation: 16,
        // titleDisplayMode: 'alwaysShow',
        titleDisplayMode: 'alwaysHide',
      },
      bottomTab: {
        iconColor: '#777777',
        textColor: '#777777',
        selectedIconColor: '#d2242a',
        selectedTextColor: '#d2242a',
        fontSize: Platform.select({ ios: 10, android: 11 }),
        selectedFontSize: Platform.select({ ios: 10, android: 11 }),
      },
      animations: {
        push: {
          enabled: animationEnabled,
        },
        pop: {
          enabled: animationEnabled,
        },
        setStackRoot: {
          enabled: animationEnabled,
          waitForRender: true,
        },
        showModal: {
          enable: animationEnabled,
        },
        dismissModal: {
          enable: animationEnabled,
        },
      },
    };
  });
}

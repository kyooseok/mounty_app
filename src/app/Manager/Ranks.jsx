// @flow
import React from 'react';
import { StyleSheet, Dimensions, Text, View } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';

import { Touchable } from 'components/Button';
import ResponsiveImage from 'components/Image/Responsive';
import RouterService from 'services/RouterService';

import Managers from './Managers';
import 'styles/implement.styl';

const LAYOUT_WIDTH = Dimensions.get('window').width;

export default function Ranks(props = {}) {
  const [index, setIndex] = React.useState(0);
  const navigationState = {
    index,
    routes: [
      { key: 'whole', title: '누적' },
      { key: 'monthly', title: '월간' },
      { key: 'weekly', title: '주간' },
    ],
  };

  const activeRoute = navigationState.routes[index];

  const renderTabBar = tabBarProps => (
    <TabBar
      {...tabBarProps}
      activeColor="white"
      inactiveColor="rgba(255,255,255,.3)"
      tabStyle={styles.tabStyle}
      indicatorStyle={styles.indicatorStyle}
      style={styles.tabBarStyle}
      pressColor={false}
      renderLabel={({ route, color }) => (
        <Text styleName="text16 medium" style={{ color }}>
          {route.title}
        </Text>
      )}
    />
  );

  const renderScene = ({ route }) => {
    return (
      <Managers
        appeared={props.appeared && route.key === activeRoute.key}
        category={route.key}
        ListHeaderComponent={
          <View styleName="pv6">
            {/*<Touchable*/}
            {/*  activeOpacity={0.9}*/}
            {/*  onPress={() =>*/}
            {/*    RouterService.showWebViewModal({*/}
            {/*      uri:*/}
            {/*        'https://cs.wlfa.us/kb/%EB%8D%94%ED%95%9C%ED%8A%B8%EB%A0%88%EC%9D%B4%EB%8D%94cts-%EC%86%8C%EA%B0%9C',*/}
            {/*    })*/}
            {/*  }>*/}
              <ResponsiveImage
                source={{
                  uri:
                    'https://thehantrader.com/images/cts_tab/banner_cts2.png',
                }}
              />
            {/*</Touchable>*/}
          </View>
        }
        ListFooterComponent={
          <View styleName="p25 ph15">
            <Text styleName="text13 regular caption">
              세븐핀테크(주)는 더한트레이더CTS플랫폼 개발 및 운영사입니다.{`\n`}
              제공하는 서비스의 운영 및 책임은 각 판매법인에 존재하며 회원과
              계약관계 또한 각 판매법인에서 독자적으로 진행됩니다. 따라서 계약과
              서비스에 관련 법적 책임소재는 각 판매법인에 존재하며 플랫폼 및
              시스템 관련 책임소재는 세븐핀테크(주)에 존재합니다
            </Text>
          </View>
        }
      />
    );
  };

  return (
    <TabView
      lazyPreloadDistance={0.3}
      navigationState={navigationState}
      onIndexChange={setIndex}
      initialLayout={{ width: LAYOUT_WIDTH }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
    />
  );
}

const styles = StyleSheet.create({
  tabBarStyle: {
    backgroundColor: '#d2242a',
    elevation: 0.5,
    paddingHorizontal: 20,
  },
  tabStyle: {
    width: 'auto',
    minHeight: 38,
    paddingVertical: 9,
    paddingLeft: 0,
    paddingRight: 16,
    justifyContent: 'flex-start',
  },
  indicatorStyle: {
    height: 0,
    // marginBottom: 6,
    backgroundColor: 'white',
  },
});

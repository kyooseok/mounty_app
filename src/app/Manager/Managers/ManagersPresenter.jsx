/**
 * @flow
 */
import React from 'react';
import { View, Image, Text } from 'react-native';

import RatioText from 'components/RatioText';
import { Touchable, TouchablePropTypes } from 'components/Button';
import stubArray from 'utils/stubArray';

import './styles.styl';

export type ManagerPropTypes = {
  page_code: string, // Primary key
  trading_txt: string[], // 상태?
  company_img: string, // 운용사 이미지
  manager_img: string, // 매니져 이미지
  cts_type: string, // 상품명
  cts_company: string, // 운용사
  cts_info: ?string, // 설명
  total_ratio: number, // 수익률
  total_profit: number, // 수익금
};

type ItemRowPropTypes = {
  item: ManagerPropTypes,
  buttonProps: TouchablePropTypes,
};
export const ItemRow = React.memo((props: ItemRowPropTypes = {}) => {
  const {
    manager_img,
    cts_type,
    cts_company,
    total_ratio,
    trading_txt,
  } = Object.assign(props.item, {
    company_img: String(props.item.company_img).replace(
      '../../',
      'https://thehantrader.com/',
    ),
    manager_img: String(props.item.manager_img).replace(
      '../../',
      'https://thehantrader.com/',
    ),
  });

  return (
    <Touchable {...props.buttonProps}>
      <View styleName="ItemRow">
        <Image source={{ uri: manager_img }} styleName="ItemRowImage mr15" />
        <View styleName="compacted">
          <View styleName="row">
            {stubArray(trading_txt).map(badge => (
              <ItemRowBadge key={badge} badge={badge} />
            ))}
          </View>
          <View styleName="ItemRowTitle">
            <Text numberOfLines={1} styleName="ItemRowTitleText">
              {cts_type}
            </Text>
          </View>
          <View styleName="ItemRowCompany">
            <Text styleName="ItemRowCompanyText">{cts_company}</Text>
          </View>
        </View>
        <View styleName="ItemRowRatio">
          <RatioText styleName="ItemRowRatioText" value={total_ratio} />
        </View>
      </View>
    </Touchable>
  );
});

type ItemRowBadgePropTypes = {
  badge: string,
};
function ItemRowBadge({ badge }: ItemRowBadgePropTypes = {}) {
  let color;

  switch (badge) {
    case '완판':
      color = '#fc033d';
      break;
    case '연동가능':
    case '검증기간':
      color = '#FF9800';
      break;
    case '운용중단':
      color = '#2196F3';
      break;
    case '연동불가':
    case '괴리율 조정':
      color = '#ccc8c9';
      break;
    default:
      color = '#ccc8c9';
      break;
  }

  return (
    <View styleName="ItemRowBadge" style={{ backgroundColor: color }}>
      <Text styleName="ItemRowBadgeText">{badge}</Text>
    </View>
  );
}

export const ItemRowSeparator = React.memo(() => (
  <View styleName="ItemRowSeparator" />
));


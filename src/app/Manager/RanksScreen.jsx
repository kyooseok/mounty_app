// @flow
import React from 'react';
import {
  Platform,
  InteractionManager,
  ToastAndroid,
  BackHandler,
  View,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import RouterService from 'services/RouterService';

import Ranks from './Ranks';

import {
  BOTTOM_TABS_ID
} from '../routeKeys';


import 'styles/implement.styl';

export default class RanksScreen extends React.PureComponent {
  static options(props = {}) {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);

    this.state = {
      appeared: true,
    };
  }

  componentDidMount(): void {
    const { componentId } = this.props;
    InteractionManager.runAfterInteractions(() => {
      MaterialCommunityIcons.getImageSource('settings', 26, '#FFFFFF').then(
        icon => {
          Navigation.mergeOptions(componentId, {
            topBar: {
              rightButtons: [
                {
                  id: 'NAVIGATION_BUTTON_SETTINGS',
                  icon: icon,
                },
              ],
            },
          });
        },
      );
    });
  }

  componentDidAppear() {
    this.setState({ appeared: true });

    BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPressed);
  }

  componentDidDisappear() {
    this.setState({ appeared: false });

    this.hardwareBackPressedTime = null;

    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.hardwareBackPressed,
    );
  }

  hardwareBackPressed() {
    Navigation.mergeOptions(BOTTOM_TABS_ID, {
      bottomTabs: {
        currentTabIndex: 0,
      },
    });

    return true;
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'NAVIGATION_BUTTON_SETTINGS') {
      return RouterService.showViewerSettingsModal();
    }
  }

  render() {
    const props = {
      ...this.props,
      appeared: this.state.appeared,
    };

    return (
      <View styleName="compacted bg-background">
        {React.createElement(Ranks, props)}
      </View>
    );
  }
}

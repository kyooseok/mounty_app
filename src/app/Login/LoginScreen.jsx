// @flow
import React from 'react';
import { InteractionManager, View, Alert } from 'react-native';
import { Navigation, ComponentEvent } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import useObservable from 'utils/hooks/useObservable';
import LoginForm from './Form';
import './styles.styl';

type LoginScreenContainerPropTypes = ComponentEvent & {};
export function LoginScreenContainer(
  props: LoginScreenContainerPropTypes = {},
) {
  const { session } = useObservable();

  const handleSubmit = input => {
    session
      .createAsync(input.username, input.password)
      .then(() => Navigation.dismissModal(props.componentId))
      .catch(e => Alert.alert('알림', e.message));
  };

  return <LoginForm handleSubmit={handleSubmit} />;
}

/**
 *
 */
export default class LoginScreen extends React.Component {
  static options() {
    return {
      topBar: {
        title: {
          text: '로그인',
        },
      },
    };
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  componentDidMount(): void {
    const { componentId, commandType } = this.props;

    if (commandType === 'showModal') {
      InteractionManager.runAfterInteractions(() => {
        MaterialCommunityIcons.getImageSource('close', 26, '#FFFFFF').then(
          icon => {
            Navigation.mergeOptions(componentId, {
              topBar: {
                rightButtons: [
                  {
                    id: 'DISMISS_MODAL',
                    icon: icon,
                  },
                ],
              },
            });
          },
        );
      });
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  render() {
    return (
      <View styleName="compacted bg-background">
        {React.createElement(LoginScreenContainer, this.props)}
      </View>
    );
  }
}

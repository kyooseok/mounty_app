'use strict';

import { Image } from 'react-native';
import { Navigation } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';

import AppCache, { ASK_NEWBIE_GUIDE_SAW } from './cache';
import registerScreen from './registerScreen';
import navigationOptions from './navigationOptions';
import {
  BROWSE_SCREEN,
  BOTTOM_TABS_ID,
  MANAGER_RANKS_SCREEN,
  // ROUTE_MAP_SCREEN,
  VIEWER_PROFILE_SCREEN,
  FOREIGN_SCREEN,
} from './routeKeys';

export function setup() {
  // 화면 등록
  registerScreen();

  return navigationOptions()
    .then(options => Navigation.setDefaultOptions(options))
    .then(() => setBottomTabsBasedLayout());
}

/**
 *
 * @returns {Promise<unknown>|Promise<any>}
 */
function setBottomTabsBasedLayout() {
  const promises = [
    MaterialCommunityIcons.getImageSource('home-variant', 32),
    MaterialCommunityIcons.getImageSource('clipboard-flow', 32),
    MaterialCommunityIcons.getImageSource('account', 36),
  ];

  return Promise.all(promises).then(icons => {
    Image.getSize(icons[0].uri, (width, height) =>
      console.log('icon size', width, height),
    );

    return Navigation.setRoot({
      root: {
        bottomTabs: {
          id: BOTTOM_TABS_ID,
          children: [
            {
              stack: {
                children: [
                  {
                    component: {
                      name: FOREIGN_SCREEN,
                    },
                  },
                ],
                options: {
                  bottomTab: {
                    text: '홈',
                    icon: icons[0],
                    selectedIcon: icons[0],
                  },
                },
              },
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: BROWSE_SCREEN,
                    },
                  },
                ],
                options: {
                  bottomTab: {
                    text: '리포트',
                    icon: icons[1],
                    selectedIcon: icons[1],
                  },
                },
              },
            },
            // Tab1
            {
              stack: {
                children: [
                  {
                    component: {
                      name: VIEWER_PROFILE_SCREEN,
                    },
                  },
                ],
                options: {
                  bottomTab: {
                    text: '계좌잔고',
                    icon: icons[2],
                    selectedIcon: icons[2],
                  },
                },
              },
            },
            // Tab2
          ],
        },
      },
    });
  });
}

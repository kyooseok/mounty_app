import { omit } from 'lodash';
import invariant from 'invariant';
import { autobind } from 'core-decorators';
import AsyncStorage from '@react-native-community/async-storage';

const RESOURCE_CONFIG = {
  CACHE_KEY_PREFIX: '@CACHE::',
  CACHE_STORED_OBJECT_KEY: 'VALUE',
};

export const ASK_ACCESS_TOKEN = 'ASK_ACCESS_TOKEN';
export const ASK_INTERLOCK_ENABLED = 'ASK_INTERLOCK_ENABLED';
export const ASK_NEWBIE_GUIDE_SAW = 'ASK_NEWBIE_GUIDE_SAW';

function genkey(key: string, prefix: ?string = null) {
  if (typeof prefix === 'string') {
    return `${prefix}${key}`;
  }
  return key;
}

function originKeyValueSerializerFn(keyValuePair: Array<[string, any]>) {
  invariant(
    Array.isArray(keyValuePair) && keyValuePair.length === 2,
    '잘못 된 값으로 들어왔어',
  );

  const [originKey, originValue] = keyValuePair;
  const cacheKey = genkey(originKey, RESOURCE_CONFIG.CACHE_KEY_PREFIX);
  const cacheValue = JSON.stringify({
    [RESOURCE_CONFIG.CACHE_STORED_OBJECT_KEY]: originValue,
  });

  return {
    cacheKey,
    originKey,
    cacheValue,
    originValue,
  };
}

function cacheKeyValueSerializerFn(cacheKeyValuePair: Array<[string, string]>) {
  invariant(
    Array.isArray(cacheKeyValuePair) && cacheKeyValuePair.length === 2,
    '잘못 된 값으로 들어왔어',
  );

  const [cacheKey, cacheValue] = cacheKeyValuePair;
  const originValueObj = JSON.parse(cacheValue);

  return {
    cacheKey,
    cacheValue,
    originKey: String(cacheKey).replace(RESOURCE_CONFIG.CACHE_KEY_PREFIX, ''),
    originValue: originValueObj?.[RESOURCE_CONFIG.CACHE_STORED_OBJECT_KEY],
  };
}

// 캐시 데이터
let dataMemory = {};

@autobind
class AppCache {
  syncPromise;

  constructor() {}

  /**
   *
   * @param {string} key
   */
  get(key: string) {
    return Object.prototype.hasOwnProperty.call(dataMemory, key)
      ? dataMemory[key]
      : undefined;
  }

  /**
   *
   * @param key
   * @param value
   * @returns {AppCache}
   */
  set(key: string | Array<[string, string]>, value?: string) {
    const {
      originKey,
      originValue,
      cacheKey,
      cacheValue,
    } = originKeyValueSerializerFn([key, value]);

    dataMemory[originKey] = originValue;

    setImmediate(() => {
      AsyncStorage.setItem(cacheKey, cacheValue, (...args) => {
        console.log('CacheService', 'AsyncStorage.set', ...args);
      });
    });

    return this;
  }

  multiSet(keyValuePairs: Array<[string, any]>) {
    invariant(Array.isArray(keyValuePairs), '타입에러!');

    const cacheKeyValuePairs = keyValuePairs.reduce((acc, keyValuePair) => {
      if (Array.isArray(keyValuePair) && keyValuePair.length === 2) {
        const {
          originKey,
          originValue,
          cacheKey,
          cacheValue,
        } = originKeyValueSerializerFn(keyValuePair);

        acc.push([cacheKey, cacheValue]);
        dataMemory[originKey] = originValue;
      }

      return acc;
    }, []);

    setImmediate(() => {
      AsyncStorage.multiSet(cacheKeyValuePairs, (...args) => {
        console.log('CacheService', 'AsyncStorage.multiSet', ...args);
      });
    });

    return this;
  }

  remove(key: string | string[]) {
    const originValue = this.get(key);

    setImmediate(() => {
      const cacheKey = genkey(key, RESOURCE_CONFIG.CACHE_KEY_PREFIX);
      return AsyncStorage.removeItem(cacheKey, error => {
        console.log('CacheService', 'AsyncStorage.remove', error);

        if (error) {
          dataMemory[key] = originValue;
        }
      });
    });

    dataMemory = omit(dataMemory, [key]);
    return this;
  }

  multiRemove(keys: string[]) {
    if (Array.isArray(keys)) {
      const cacheKeys = keys.reduce((acc, key) => {
        const cacheKey = genkey(key, RESOURCE_CONFIG.CACHE_KEY_PREFIX);
        acc.push(cacheKey);

        return acc;
      }, []);

      dataMemory = omit(dataMemory, keys);
      setImmediate(() => AsyncStorage.multiRemove(cacheKeys));
    }

    return this;
  }

  /**
   *
   * @param callback
   * @private
   */
  _getCachedKeyValue(
    callback: (error: ?Error, keyValuePairs: Array<[string, string]>) => void,
  ) {
    callback = typeof callback === 'function' ? callback : () => null;

    AsyncStorage.getAllKeys((error, keys) => {
      if (error) {
        return callback(error, null);
      }

      // 접두어를 사용해서 해당 서비스로 저장 된 키값을 가져옴
      const cachedKeys = keys.filter(key =>
        key.startsWith(RESOURCE_CONFIG.CACHE_KEY_PREFIX),
      );

      AsyncStorage.multiGet(cachedKeys, callback);
    });
  }

  synchronize() {
    if (!this.syncPromise) {
      this.syncPromise = new Promise((resolve, reject) => {
        this._getCachedKeyValue((error, keyValuePairs) => {
          if (error) {
            reject(error);
          }

          keyValuePairs.forEach(cacheKeyValuePair => {
            const { originKey, originValue } = cacheKeyValueSerializerFn(
              cacheKeyValuePair,
            );

            if (originKey && originValue) {
              dataMemory[originKey] = originValue;
            }
          });

          resolve(null);
        });
      });
    }

    return this.syncPromise;
  }
}

export default new AppCache();

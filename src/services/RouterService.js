import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import InAppBrowser from 'react-native-inappbrowser-reborn';

import { autobind } from 'core-decorators';
import {
  LOGIN_SCREEN,
  WEB_VIEW_SCREEN,
  ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN,
  CONTACTS_SCREEN,
  VIEWER_SETTINGS_SCREEN,
  VIEWER_TRANSACTIONS_SCREEN,
} from 'app/routeKeys';

import stubObject from 'utils/stubObject';

@autobind
class RouterService {
  // navigateToHome(componentId: string = ROOT_STACK_ID) {
  //   return Navigation.push(componentId, {
  //     component: {
  //       name: HOME_SCREEN,
  //     }
  //   })
  // }

  showWebViewModal(passProps = {}, options = {}) {
    return showModal(WEB_VIEW_SCREEN, passProps, options);
  }

  showLoginModal(passProps = {}, options = {}) {
    return showModal(LOGIN_SCREEN, passProps, options);
  }

  showManagerModal(passProps = {}, options = {}) {
    return showModal(ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN, passProps, options);
  }

  showContactsModal(passProps = {}, options = {}) {
    return showModal(CONTACTS_SCREEN, passProps, options);
  }

  showViewerTransactionsModal(passProps = {}, options = {}) {
    return showModal(VIEWER_TRANSACTIONS_SCREEN, passProps, options);
  }

  showViewerSettingsModal(passProps = {}, options = {}) {
    return showModal(VIEWER_SETTINGS_SCREEN, passProps, options);
  }

  async openInAppBrowser(url) {
    const inAppBrowserParams = Platform.select({
      ios: {
        dismissButtonStyle: 'close',
        // preferredBarTintColor: ColorPalette.get('gray100'),
        // preferredControlTintColor: ColorPalette.get('gray20'),
        readerMode: false,
        animated: true,
        modalPresentationStyle: 'overFullScreen',
        // modalTransitionStyle: 'partialCurl',
        modalEnabled: false,
        enableBarCollapsing: true,
      },
      android: {
        showTitle: true,
        toolbarColor: '#d2242a',
        // secondaryToolbarColor: 'white',
        enableUrlBarHiding: true,
        enableDefaultShare: true,
        forceCloseOnRedirection: false,
        // Specify full animation resource identifier(package:anim/name)
        // or only resource name(in case of animation bundled with app).
        animations: {
          // startEnter: 'fade_in',
          // startExit: 'fade_out',
          endEnter: 'slide_in_left',
          endExit: 'slide_out_right',
        },
        // headers: {
        //   'my-custom-header': 'my custom header value'
        // },
      },
    });

    InAppBrowser.isAvailable()
      .then(() => InAppBrowser.open(url, inAppBrowserParams))
      .catch(e => {
        // 브라우져를 종료시킴
        InAppBrowser.close();
        // TODO. 에러수집
        console.warn(e);

        // 다시 시도시킴
        return InAppBrowser.open(url, inAppBrowserParams);
      });
  }
}

export function showModal(componentName: string, passProps = {}, options = {}) {
  return Navigation.showModal({
    stack: {
      children: [
        {
          component: {
            name: componentName,
            passProps: Object.assign(
              { commandType: 'showModal' },
              stubObject(passProps),
            ),
            options: Object.assign(
              {
                modalPresentationStyle: 'overFullScreen',
                modal: {
                  swipeToDismiss: true,
                },
              },
              stubObject(options),
            ),
          },
        },
      ],
    },
  });
}

export default new RouterService();

'use strict';

import Frisbee from 'frisbee';
import { autobind } from 'core-decorators';

// type RestClientOptionTypes = {
//   method?: string, // 기본 값은 GET
//   headers?: string, // {}
//   timeout?: number,
// };
//
// interface RestClientServiceInterface {
//   authKey: string; // 인증 값
//
//   setAuthKey(authKey: string): void;
//
//   makeRequest(): Promise<any>;
//   makeRequestURL(path: string, params: any): string;
//   makeRequestOptions(): Promise<any>;
// }
//
@autobind
export class RestClientService {
  client;

  constructor() {
    this.client = new Frisbee({
      baseURI: 'http://3.34.36.88',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      logResponse(...args) {
        console.log('RestClientService.response', ...args);
      },
    });
  }

  /**
   *
   * @param options
   *
   * @returns {RestClientService}
   */
  mergeOptions(options = {}) {
    this.client.setOptions(options);

    return this;
  }

  /**
   *
   * @param jwtToken
   *
   * @returns {RestClientService}
   */
  setJwtToken(jwtToken: string) {
    this.client.jwt(jwtToken);

    return this;
  }

  abort(token: string) {
    return this.client.abort(token);
  }

  abortAll() {
    return this.client.abortAll();
  }

  get(path: string, options = {}) {
    return this.client.get(path, options);
  }

  head(path: string, options = {}) {
    return this.client.head(path, options);
  }

  post(path: string, options = {}) {
    return this.client.post(path, options);
  }

  put(path: string, options = {}) {
    return this.client.put(path, options);
  }

  del(path: string, options = {}) {
    return this.client.del(path, options);
  }

  delete(path: string, options = {}) {
    return this.client.delete(path, options);
  }

  patch(path: string, options = {}) {
    return this.client.patch(path, options);
  }
}

export default new RestClientService();

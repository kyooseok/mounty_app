import * as Yup from 'yup';
import { isBoolean, isArray } from 'lodash'

const CELLPHONE_VALID_REGEX = /^\d{3}\d{3,4}\d{4}$/;
const SPECIAL_CHARS = [
  '!', '@', '#', '$', '%', '^', '&', '*',
  '(', ')', '+', '=', '-', '_', '{', '}',
  '[', ']', ',', '.', '<', '>', '?', '|',
];

// https://github.com/jaredpalmer/formik/issues/90#issuecomment-354873201
export function isEmail(val) {
  return Yup.string().email(null).isValidSync(val);
}

export function isCellphone(val) {
  return CELLPHONE_VALID_REGEX.test(val);
}

/**
 * 휴대전화번호 체크
 *
 * Yup.string().cellphone(message)
 */
Yup.addMethod(Yup.string, 'cellphone', function(message) {
  return this.test('test-cellphone', message, function (value) {
    // 휴대전화번호 체크
    if( isCellphone(value) ) return true;
    return this.createError(this.path, message);
  });
});

/**
 * 사용자 이름 체크 (이메일, 휴대전화번호)
 *
 * Yup.string().username(message, ['email', 'cellphone'])
 */
Yup.addMethod(Yup.string, 'username', function(message, formats=['email']) {
  return this.test('test-username', message, function (value) {
    // 이메일 체크
    if( formats.includes('email') && isEmail(value) ) {
      return true;
    }

    // 휴대전화번호 체크
    if( formats.includes('cellphone') && isCellphone(value) ) {
      return true;
    }

    return this.createError(this.path, message);
  });
});

/**
 * 비밀번호 확인
 *
 * Yup.string().password(message, ['lowercase', 'number'])
 * lowercase
 * uppercase
 * number
 * special
 */
Yup.addMethod(Yup.string, 'password', function(message, inclusions=['lowercase', 'number']) {
  return this.test('test-password', message, function (value) {
    const flagMap = (isArray(inclusions)? inclusions: ['lowercase', 'number'])
      .reduce((acc, cur) => {
        acc[cur] = false;
        return acc;
      }, {});

    if( /[0-9]/.test(value) ) isBoolean(flagMap.number) && (flagMap.number = true);
    if( /[a-z]/.test(value) ) isBoolean(flagMap.lowercase) && (flagMap.lowercase = true);
    if( /[A-Z]/.test(value) ) isBoolean(flagMap.uppercase) && (flagMap.uppercase = true);

    if( new RegExp(`(${SPECIAL_CHARS.map(v=>`\\${v}`).join('|')})`).test(value)) {
      isBoolean(flagMap.special) && (flagMap.special = true);
    }

    const isValid = Object.values(flagMap).reduce((acc, cur) => {
      if( acc === false ) return false;
      return cur;
    }, true);

    return isValid? true: this.createError(this.path, message);
  });
});

export default Yup;

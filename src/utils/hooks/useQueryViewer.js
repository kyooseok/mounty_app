import { sortBy } from 'lodash';
import { useQuery } from 'react-query';
import { BalanceService } from 'services/apis/endpoints/accounts';

const RESOURCE_CONFIG = {
  QUERY_KEY: 'VIEWER_QUERY',
};

function queryFn(queryKey, ...queryVariables) {
  const promises = [BalanceService.getAsync(), BalanceService.queryAsync()];

  return Promise.all(promises).then(responses => {
    const { uid, name, phone, security, ...balance } = responses[0];

    let portfolio = responses[1];
    portfolio.items = sortBy(portfolio.items, 'valuation_amount').reverse();

    return {
      account_no: uid,
      username: name,
      cellphone: phone,
      security: security,
      balance,
      portfolio,
    };
  });
}

export default function useQueryViewer(queryConfig = {}) {
  return useQuery(RESOURCE_CONFIG.QUERY_KEY, queryFn, queryConfig);
}

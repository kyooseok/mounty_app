import React from 'react';
import { isEmpty, isObject } from 'lodash';

import stubObject from 'utils/stubObject';

export default function useObjectState(object = {}) {
  const [state, setState] = React.useState(stubObject(object));

  return [
    state,
    nextState => {
      if (typeof nextState === 'function') {
        return setState(nextState);
      }

      return setState(prevState => {
        if (isObject(nextState)) {
          const changeState = Object.keys(nextState).reduce((acc, key) => {
            if (nextState[key] !== prevState[key]) {
              acc[key] = nextState[key];
            }
            return acc;
          }, {});

          if (!isEmpty(changeState)) {
            return Object.assign({}, prevState, nextState);
          }
        }
        return prevState;
      });
    },
  ];
}

import { isString } from 'lodash';

export default function stubString(origin=null, placeholder='') {
  if( isString(origin) ) return origin;
  return placeholder;
}

import React from 'react';
import { SafeAreaView, View, Text } from 'react-native';
import Button from './Button';
import { storiesOf } from '@storybook/react-native';

storiesOf('Button', module).add('text', () => (
  <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Button mode="outlined" onPress={() => null}>
      <Text>하이</Text>
    </Button>
    <Button mode="contained" onPress={() => null}>
      <Text>테스트</Text>
    </Button>
    <Button mode="contained" loading onPress={() => null}>
      <Text>테스트</Text>
    </Button>
    <Button mode="outlined" compacted loading onPress={() => null}>
      <Text>테스트</Text>
    </Button>
  </SafeAreaView>
));

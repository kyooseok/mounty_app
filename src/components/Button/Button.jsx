/**
 * @author Martin, Lee
 *
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  ActivityIndicator,
  View,
  Text,
  TouchableOpacityProps,
} from 'react-native';

import Touchable from './Touchable';

const RESOURCE_CONFIG = {
  DEFAULT_HEIGHT: 48,
  DEFAULT_TEXT_COLOR: '#000000',
  DEFAULT_BORDER_COLOR: '#EEEEEE',
  DEFAULT_BACKGROUND_COLOR: '#F2F2F2',
};

const styles = StyleSheet.create({
  content: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 12,
    paddingRight: 12,
  },
  icon: {
    width: 16,
    marginLeft: 2,
    marginRight: 8,
  },
});

type ButtonPropTypes = TouchableOpacityProps & {
  mode?: 'text' | 'outlined' | 'contained', // text
  compacted?: boolean,
  loading?: boolean,
  height?: number, // 48
  tintColor?: string, // DEFAULT: gray?
  children: React$ElementType,
};
export default function Button(props: ButtonPropTypes = {}) {
  const {
    mode,
    height,
    loading,
    compacted,
    buttonColor,
    textColor,
    children,
    buttonProps,
  } = normalizeProps(props);

  let buttonStyle = {
    height,
    minWidth: compacted ? 'auto' : 64,
  };

  switch (mode) {
    case 'text':
      break;
    case 'outlined':
      buttonStyle.borderWidth = StyleSheet.hairlineWidth;
      buttonStyle.borderColor = buttonColor;
      break;
    case 'contained':
      buttonStyle.backgroundColor = buttonColor;
      break;
  }

  return (
    <Touchable {...buttonProps} style={[buttonStyle, buttonProps?.style]}>
      <View style={[styles.content]}>
        {loading ? (
          <ActivityIndicator size={16} color={textColor} style={styles.icon} />
        ) : null}
        <Text numberOfLines={1}>{children}</Text>
      </View>
    </Touchable>
  );
}

/**
 *
 * @param props
 */
function normalizeProps(props = {}) {
  const {
    mode,
    height,
    tintColor,
    compacted,
    loading,
    children,
    ...buttonProps
  } = props;

  const buttonMode = ['text', 'outlined', 'contained'].includes(props.mode)
    ? props.mode
    : 'text';

  const buttonColor = tintColor
    ? tintColor
    : buttonMode === 'text'
    ? RESOURCE_CONFIG.DEFAULT_TEXT_COLOR
    : buttonMode === 'outlined'
    ? RESOURCE_CONFIG.DEFAULT_BORDER_COLOR
    : RESOURCE_CONFIG.DEFAULT_BACKGROUND_COLOR;

  return {
    mode: buttonMode,
    height: height || RESOURCE_CONFIG.DEFAULT_HEIGHT,
    compacted: Boolean(compacted),
    loading: Boolean(loading),
    buttonProps,
    buttonColor,
    textColor: 'black',
    children,
  };
}

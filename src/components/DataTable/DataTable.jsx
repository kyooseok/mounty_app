/**
 * DataTable
 * DataTable.Row
 * DataTable.Cell
 *
 * @flow
 */

import React from 'react';
import { View } from 'react-native';

import styles from './styles';

export default function DataTable(props = {}) {
  const { children, style, ...viewProps } = props;

  return (
    <View {...viewProps} style={[styles.container, style]}>
      {children}
    </View>
  );
}

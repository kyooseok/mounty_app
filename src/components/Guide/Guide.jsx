// @flow
import React from 'react';
import { View } from 'react-native';
// import ViewPager from '@react-native-community/viewpager';
// import { times } from 'lodash';
//
// import Circle from './Circle';
// import GuideScene_1 from './GuideScene_1';
// import GuideScene_2 from './GuideScene_2';
// import GuideScene_3 from './GuideScene_3';
import GuideScene_4 from './GuideScene_4';

import './styles.styl';

export default function Guide(props) {
  // const [position, setPosition] = React.useState(0);
  // const [interactionReady, setInteractionReady] = React.useState(false);

  // React.useEffect(() => {
  //   requestAnimationFrame(() => setInteractionReady(true));
  // }, []);

  return (
    <View styleName="compacted">
      <GuideScene_4
        secondary={'그 동안 경험하지 못했던\n혁신 핀테크 서비스'}
      />
    </View>
  );
}

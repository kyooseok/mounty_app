// @flow
import React from 'react';
import {StyleSheet, Dimensions, Text, View, Image} from 'react-native';

import './styles.styl';

const LAYOUT_WIDTH = Dimensions.get('window').width;

type GuideSceneProps = {
  primary: string;
  secondary?: string;
}

export default function GuideScene_2({primary, secondary}: GuideSceneProps) {
  return (
    <View styleName="GuideScene">
      <View styleName="GuideSceneContainer">
        <View styleName="GuideScenePrimary">
          <Text styleName="GuideScenePrimaryText">{primary}</Text>
        </View>
        <View styleName="GuideSceneSecondary">
          <Text styleName="GuideSceneSecondaryText">{secondary}</Text>
        </View>

        <View styleName="compacted">
          <View style={[StyleSheet.absoluteFillObject, {overflow: 'hidden'}]}>
            <Image source={require('./guide-scene-1.png')} style={{position: 'absolute', left: (LAYOUT_WIDTH - 460) + -LAYOUT_WIDTH, top: -50, bottom: 0,}} />
          </View>
        </View>
      </View>
    </View>
  )
}

/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

const { getDefaultConfig } = require('metro-config');

module.exports = (async function() {
  const { resolver } = await getDefaultConfig();

  return {
    transformer: {
      babelTransformerPath: require.resolve('./react-native.transformer'),
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: false,
        },
      }),
    },
    resolver: {
      sourceExts: [...resolver.sourceExts, 'styl', 'jsx', 'html'],
    },
  };
})();
